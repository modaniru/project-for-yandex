package com.modaniru.project.controller;

import com.modaniru.project.dao.ItemRepos;
import com.modaniru.project.entity.Item;
import com.modaniru.project.objects.Container;
import com.modaniru.project.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;


@RestController
@RequestMapping("/")
public class MainController {
    @Autowired
    private ItemService itemService;


    @PostMapping("/imports")
    public void saveAllItems(@RequestBody List<Container> containers){
        for(Container c: containers){
            for(Item item: c.getItems()){
                item.setDate(c.getUpdateDate());
            }
            itemService.saveAll(c.getItems());
        }
        List<Item> categories = itemService.findAllCategoriesByContainer(containers);
        for(Item category: categories){
            List<Item> offers = itemService.findAllOffers(category.getId());
            if(offers.isEmpty()) continue;
            int count = offers.size();
            int sum = 0;
            for(Item offer: offers){
                sum += offer.getPrice();
            }
            category.setPrice(sum/count);
        }
        itemService.saveAll(categories);

    }
    @GetMapping("/nodes/{id}")
    public Item getItem(@PathVariable UUID id){
        Item item = itemService.findById(id);
        item.setChildren(itemService.findAllItems(item.getId()));
        return item;
    }
    @DeleteMapping("/delete/{id}")
    public void deleteItem(@PathVariable UUID id){
        Item item = itemService.findById(id);
        List<Item> itemList = new ArrayList<>(itemService.findAllItems(item.getId()));
        for(int i = 0; i < itemList.size(); i++){
            itemList.addAll(itemService.findAllItems(itemList.get(i).getId()));
        }
        itemList.add(item);
        itemService.deleteAllItems(itemList);
    }
    @GetMapping("/sales/{date}")
    public List<Item> getItemLessThanDayDateUpdate(@PathVariable String date){
        ZonedDateTime zonedDateTime = ZonedDateTime.parse(date).minusDays(1);
        System.out.println(zonedDateTime);
        return itemService.findItemsByDate(zonedDateTime);
    }
}
