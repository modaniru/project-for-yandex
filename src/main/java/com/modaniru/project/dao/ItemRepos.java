package com.modaniru.project.dao;

import com.modaniru.project.entity.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;

@Repository
public interface ItemRepos extends JpaRepository<Item, UUID> {
    List<Item> findAllByParentIdAndType(UUID parentId, String type);
    List<Item> findAllByParentId(UUID parentId);
    List<Item> findAllByDateBetweenAndType(ZonedDateTime date, ZonedDateTime date2, String type);
}
