package com.modaniru.project.objects;

import com.modaniru.project.entity.Item;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

public class Container {
    private ZonedDateTime updateDate;
    private List<Item> items;

    public Container() {
    }

    public Container(ZonedDateTime zonedDateTime, List<Item> items) {
        this.updateDate = zonedDateTime;
        this.items = items;
    }

    public ZonedDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(ZonedDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
