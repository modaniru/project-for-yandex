package com.modaniru.project.service;

import com.modaniru.project.dao.ItemRepos;
import com.modaniru.project.entity.Item;
import com.modaniru.project.objects.Container;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.*;

@Service
public class ItemService {
    @Autowired
    private ItemRepos itemRepos;

    public void saveAll(List<Item> items){
        itemRepos.saveAll(items);
    }

    public List<Item> findAllOffers(UUID parentId){
        List<Item> categories = itemRepos.findAllByParentIdAndType(parentId, "CATEGORY");
        List<Item> offers = itemRepos.findAllByParentIdAndType(parentId, "OFFER");
        for(int i = 0; i < categories.size(); i++){
            categories.addAll(itemRepos.findAllByParentIdAndType(categories.get(i).getId(), "CATEGORY"));
            offers.addAll(itemRepos.findAllByParentIdAndType(categories.get(i).getId(), "OFFER"));
        }
        return offers;
    }

    public List<Item> findAllCategoriesByContainer(List<Container> containers){
        HashSet<Item> hashSetItems = new HashSet<>();
        for(Container c: containers){
            List<Item> categories = new ArrayList<>();
            for(Item i: c.getItems()){
                if(i.getType().equals("CATEGORY")){
                    categories.add(i);
                }
            }
            hashSetItems.addAll(categories);
        }
        return new ArrayList<>(hashSetItems);
    }
    public List<Item> findAllItems(UUID parentId){
        List<Item> items = itemRepos.findAllByParentId(parentId);
        List<Item> temporary = new ArrayList<>(items);
        for(int i = 0; i < temporary.size(); i++){
            List<Item> children = itemRepos.findAllByParentId(temporary.get(i).getId());
            temporary.get(i).setChildren(children);
            temporary.addAll(children);
        }
        return items;
    }

    public Item findById(UUID id){
        Optional<Item> optionalItem = itemRepos.findById(id);
        if(!optionalItem.isPresent())
            throw new IllegalArgumentException("not found item with id: "+id);
        return optionalItem.get();
    }

    public void deleteAllItems(List<Item> items){
        itemRepos.deleteAll(items);
    }

    public List<Item> findItemsByDate(ZonedDateTime zonedDateTime){
        return itemRepos.findAllByDateBetweenAndType(zonedDateTime, zonedDateTime.plusDays(1), "OFFER");
    }
}
